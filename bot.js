const NCC = require("./ncc")
const fetch = require("node-fetch")
const Keys = require("./keys")

class BotApi {
    constructor(pubkey) {
        // this.url = `http://${window.location.hostname}:8080`
        // this.url = `http://boot0.test.neurochaintech.io:8080`
        this.url = `https://api.testnet.neurochaintech.io`
        this.pubkey = pubkey
    }

    async validate() {
        const url = this.url
        const pubkey = this.pubkey
        try {
            const resp = fetch(`${url}/validate`, {
                method: "POST",
                body: JSON.stringify({rawData: pubkey})
            })

            const data = await resp
            return data.status
        } catch (e) {
            return Promise.reject({message: `can't connect to bot ${url}`})
        }
    }

    async balance() {
        const url = this.url
        const pubkey = this.pubkey
        try {
            const resp = fetch(`${url}/balance`, {
                method: "POST",
                body: JSON.stringify({rawData: pubkey})
            })
            const data = (await resp).json()
            const rawNcc = (await data).value
            return new NCC(rawNcc)
        } catch (e) {
            console.error(e)
            return Promise.reject({message: `Can't fetch balance for address ${pubkey}`})
        }
    }

    async transaction(...transaction) {
        const url = this.url
        const pubkey = this.pubkey
        const fee = 0
        const resp = fetch(`${url}/create_transaction`, {
            method: "POST",
            body: JSON.stringify({
                key_pub: {rawData: pubkey},
                outputs: transaction,
                fee
            })
        })
        return (await resp).text()
    }

    async sendTransaction(payload, signature) {
        const url = this.url
        const pubkey = this.pubkey
        try {
            const resp = await fetch(`${url}/publish`, {
                method: "POST",
                body: JSON.stringify({
                    transaction: payload,
                    signature: signature,
                    keyPub: {rawData: pubkey}
                }),
            })
            if (!resp.ok) {
                return Promise.reject(await resp.text())
            } else {}
	            return await resp.json()
        } catch (e) {
            return Promise.reject(e)
        }
    }

    async status() {
        const url = this.url
        try {
            const resp = fetch(`${url}/status`)
            const data = (await resp).json()
            return data
        } catch (e) {
            return Promise.reject(e)
        }
    }

    async send(tr, privateKey) {
        try {
            const payload = await this.transaction(tr)
            const signature = Keys.signPayload(payload, privateKey)
            const transaction = await this.sendTransaction(payload, signature)
            return transaction.id
        } catch (e) {
            return Promise.reject(e)
        }
    }
}

module.exports = BotApi

