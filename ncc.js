class NCC {
    static get _conversionRate() {return 1e9}
    constructor(petitNCC) {

        this.petitNCC = petitNCC
    }

    get ncc() {
        return this.petitNCC / NCC._conversionRate
    }

    get petit() {
        return this.petitNCC
    }

    static toPetit(ncc) {
        const petitNcc = parseFloat(ncc) * NCC._conversionRate
        if (Math.trunc(petitNcc) !== petitNcc) {
            throw {message: "can't subdivide an NCC so much"}
        }
        return new NCC(petitNcc)
    }
}

module.exports = NCC
